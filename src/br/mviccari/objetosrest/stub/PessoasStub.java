/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.mviccari.objetosrest.stub;

import br.mviccari.objetosrest.beans.Pessoa;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Jersey REST client generated for REST resource:Pessoas [Pessoas]<br>
 * USAGE:
 * <pre>
 *        PessoasStub client = new PessoasStub();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Avell G1711 NEW
 */
public class PessoasStub {
    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/ObjetosRest/webresources";

    public PessoasStub() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("Pessoas");
    }

    public Pessoa pegaPessoa() throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("PegaPessoa");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(Pessoa.class);
    }

    public Pessoa alteraPessoa(Pessoa requestEntity) throws ClientErrorException {
        return webTarget.path("AlteraPessoa").request(javax.ws.rs.core.MediaType.APPLICATION_XML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML), Pessoa.class);
    }

    public void close() {
        client.close();
    }
    
}
