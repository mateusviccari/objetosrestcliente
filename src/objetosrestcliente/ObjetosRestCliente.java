/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objetosrestcliente;

import br.mviccari.objetosrest.beans.Pessoa;
import br.mviccari.objetosrest.stub.PessoasStub;
import javax.swing.JOptionPane;

/**
 *
 * @author Avell G1711 NEW
 */
public class ObjetosRestCliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PessoasStub stub = new PessoasStub();
        Pessoa p = stub.pegaPessoa();
        Pessoa pAlterada = stub.alteraPessoa(p);
        JOptionPane.showMessageDialog(null, "Pessoa retornada: "+p);
        JOptionPane.showMessageDialog(null, "Pessoa alterada: "+pAlterada);
    }
    
}
